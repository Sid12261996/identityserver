﻿using System;
using System.ComponentModel.DataAnnotations;
using IdentityServer.Entities.Enums;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace IdentityServer.Entities
{
    public class ApplicationUser: IdentityUser
    {
        //[JsonIgnore]
        //public string Password { get; set; }

        public DateTime? LastLogin { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(100)]
        public string LastName { get; set; }
        [StringLength(500)]
        public string Email { get; set; }

        [StringLength(50)]
        public string Mobile { get; set; }

        [StringLength(500)]
        public string City { get; set; }



        [JsonIgnore]
        [StringLength(500)]
        public string EmailLower { get; set; }

        [JsonIgnore]
        [StringLength(4000)]
        public virtual string Password { get; set; }

        [StringLength(4000)]
        public string TimeZone { get; set; }

        public Roles Roles { get; set; }

        public long? CreatedBy { get; set; }
        public long OrganizationId { get; set; }

        public Organisation Organization { get; set; }

        [StringLength(4000)]
        public string Language { get; set; }

        [StringLength(4000)]
        public string Currency { get; set; }


        //public string Identity
        //{
        //    get { return Id.ToString(); }
        //}

        public string Name
        {
            get { return FirstName + " " + LastName; }
        }


        public bool IsActive { get; set; }

        /// <summary>
        /// This has to be Unique Id across system.
        /// This will be unique identity of the user through which user can be identified with in the system.
        /// </summary>
        //[In]
        public Guid UserGuid { get; set; }

        /// <summary>
        /// If student self registers himself within Logisch organization 
        /// then this information will be used to create user license renewal date.
        /// </summary>
        public DateTime? LicenseEndDate { get; set; }

        /// <summary>
        /// This will be used if its a self register user.
        /// </summary>
        public LicenseType License { get; set; }
        ///<summary>
        ///This attributes represents the user is mapped with which reseller.
        ///</summary>
        public long? ResellerId { get; set; }
        ///<summary>
        ///This attributes represents the reseller's coupon code
        ///</summary>
        [StringLength(500)]
        public string CouponCode { get; set; }
    }
}
