﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace IdentityServer.Entities
{
   public class DbEntity
    {
        public virtual Int64 Id { get; set; }

        [JsonIgnore]
        public virtual bool IsDeleted { get; set; }

        public virtual DateTime CreatedDateUTC { get; set; }

        [JsonIgnore]
        public virtual DateTime? UpdatedDateUTC { get; set; }
    }
}
