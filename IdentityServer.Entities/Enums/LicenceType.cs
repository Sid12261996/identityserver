﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityServer.Entities.Enums
{
    public enum LicenseType
    {
        Unknown = 0,

        /// <summary>
        /// Organization has trial license. In this license it will have access to all modules but will only be valid for 15 days or days configured in Db.
        /// </summary>
        Trial = 1,

        /// <summary>
        /// Monthly renewal policy applied.
        /// </summary>
        Monthly = 2,

        /// <summary>
        /// License needs to be renewed half yearly.
        /// </summary>
        HalfYearly = 3,

        /// <summary>
        /// License needs to be renewed yearly.
        /// </summary>
        Yearly = 4
    }
}
