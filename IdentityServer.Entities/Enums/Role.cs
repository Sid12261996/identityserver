﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityServer.Entities.Enums
{
    public enum Roles
    {
        Unknown = 0,

        ///// <summary>
        ///// User who creates an organization in EdPlan
        ///// </summary>
        //OrganizationAdmin = 1,

        ///// <summary>
        ///// Student who wants to take courses.
        ///// </summary>
        //Student = 2,

        ///// <summary>
        ///// Faculty who is responsible to create classes and look after assessments.
        ///// </summary>
        //Faculty = 3,

        ///// <summary>
        ///// Parent of student who wants to track his student activity.
        ///// </summary>
        //Parent = 4

        /// <summary>
        /// Group admin of an organization type Group.
        /// </summary>
        GroupAdmin = 1,

        /// <summary>
        /// Admin of an organization.
        /// </summary>
        OrganizationAdmin = 2,

        /// <summary>
        /// Academic Admin
        /// </summary>
        AcademicAdmin = 3,

        /// <summary>
        /// Feedback Admin
        /// </summary>
        FeedbackAdmin = 4,

        /// <summary>
        /// Finance Admin
        /// </summary>
        FinanceAdmin = 5,

        /// <summary>
        /// Hospital admin
        /// </summary>
        HospitalAdmin = 6,

        /// <summary>
        /// Forum admin
        /// </summary>
        ForumAdmin = 7,

        /// <summary>
        /// Extra curricular admin
        /// </summary>
        ExtraCurricularAdmin = 8,

        /// <summary>
        /// Administration admin
        /// </summary>
        AdministrationAdmin = 9,

        ExamController = 10,

        HeadOfDepartment = 11,

        TimeTableAdmin = 12,

        CourseAdmin = 13,

        /// <summary>
        /// Document Management System Admin
        /// </summary>
        DMSAdmin = 14,

        UserManagementAdmin = 15,

        LibraryAdmin = 16,

        /// <summary>
        /// Inventory Management System Admin
        /// </summary>
        IMSAdmin = 17,

        /// <summary>
        /// Vehicle Management System Admin.
        /// </summary>
        VMSAdmin = 18,

        HostelAdmin = 19,

        /// <summary>
        /// Admin responsible for adminmissions for the students.
        /// </summary>
        AdminmissionAdmin = 20,

        /// <summary>
        /// Human Resource Management Admin.
        /// </summary>
        HRMAdmin = 21,

        CourierAdmin = 22,

        /// <summary>
        /// Knowledge Management System Admin
        /// </summary>
        KMSAdmin = 23,

        /// <summary>
        /// Non teachning staff.
        /// </summary>
        Staff = 24,

        Faculty = 25,

        Student = 26,

        Parent = 27,

        /// <summary>
        /// Once student completes it study from an organization then he becomes alumni of that organization.
        /// </summary>
        Alumni = 28,

        /// <summary>
        /// This role is specifically used for Schools.
        /// </summary>
        SecurtyHead = 29,

        /// <summary>
        /// Student specific for test series student.
        /// </summary>
        TestSeriesStudent = 30,
        ///<summary>
        ///This role is used for Reseller  
        /// </summary>
        Reseller = 31
    }
}
